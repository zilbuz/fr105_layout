# fr105_layout

Layout for a 105 keys fr keyboard, for the pc-keyboard crate (https://crates.io/crates/pc-keyboard).

**/!\ This crate is unsupported. /!\\**

Add to `Cargo.toml` dependencies like this:
```toml
[dependencies]
fr105_layout = { git = "https://gitlab.com/zilbuz/fr105_layout.git" }
```
